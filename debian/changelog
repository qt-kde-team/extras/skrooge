skrooge (25.1.0-1) unstable; urgency=medium

  * Team upload.

  [ Aurélien COUDERC ]
  * New upstream release (25.1.0).
  * Limit Qt WebEngine dependency to architectures where it’s available
    (amd64 arm64 armhf i386).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 20 Jan 2025 22:48:34 +0100

skrooge (2.33.8-1) experimental; urgency=medium

  * Team upload.

  [ Pino Toscano ]
  * Drop the kinit dependency from skrooge, as kinit has not been needed for
    a long time.

  [ Aurélien COUDERC ]
  * Add/update Stéphane MANKOWSKI’s key in upstream keyring.
  * New upstream pre-release (2.33.8).
  * Build against Qt6, update build-deps and deps with the info from cmake.
  * Update the list of installed files.
  * Build with hardening=+all build hardening flag.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 05 Jan 2025 23:39:56 +0100

skrooge (2.33.0-1) unstable; urgency=medium

  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - bump almost all the KF packages to 5.90.0
    - drop libqca-qt5-2-dev, no more used
  * Drop the libqca-qt5-2-plugins dependency from skrooge, as QCA is no more
    used.

 -- Pino Toscano <pino@debian.org>  Wed, 25 Sep 2024 07:32:56 +0200

skrooge (2.32.0-1) unstable; urgency=medium

  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - add qtquickcontrols2-5-dev
  * Switch the transitional pkg-config build dependency to pkgconf.
  * Use QtWebEngine where available, dropping the support for QtWebKit (which
    was disabled by default upstream since 2.30.0):
    - add the qtwebengine5-dev build dependency on almost all the architectures
      in which it is available
    - drop the libqt5webkit5-dev build dependency
    - pass -DSKG_WEBKIT=OFF to cmake to explicitly disable QtWebKit
    - pass -DSKG_WEBENGINE=ON or OFF depending on whether QtWebEngine is
      available on the current architecture
  * Bump Standards-Version to 4.7.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Wed, 01 May 2024 07:49:37 +0200

skrooge (2.31.0-1) unstable; urgency=medium

  * New upstream release.

 -- Pino Toscano <pino@debian.org>  Thu, 05 Oct 2023 19:21:47 +0200

skrooge (2.30.0-2) unstable; urgency=medium

  * Get the right grantlee dependency for the shipped plugin:
    - add the dh-sequence-grantlee build dependency
    - use the ${grantlee:Depends} substvar

 -- Pino Toscano <pino@debian.org>  Sat, 05 Aug 2023 19:20:22 +0200

skrooge (2.30.0-1) unstable; urgency=medium

  * New upstream release.
  * Update the patches:
    - upstream_Revert-cmake-issue.patch: drop, backported from upstream
    - upstream_cmake-drop-bogus-CMAKE_TRY_COMPILE_TARGET_TYPE.patch:
      drop, backported from upstream
  * Pass -DSKG_WEBENGINE=OFF to cmake to keep using QtWebKit for now.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Mon, 03 Jul 2023 08:26:38 +0200

skrooge (2.29.0-1) unstable; urgency=medium

  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - bump almost all the KF packages to 5.86.0
    - add libqt5xmlpatterns5-dev
  * Bump Standards-Version to 4.6.2, no changes required.
  * Modernize building:
    - add the dh-sequence-kf5 build dependency to use the kf5 addon
      automatically, removing pkg-kde-tools
    - add the dh-sequence-python3 build dependency to use the python3 addon
      automatically, removing dh-python
    - drop all the manually specified addons and buildsystem for dh
  * Backport upstream commit 309c0d74d210e77635d8b3d6189dffae887380df, and
    commit 5cfbfaaaa7c6e46a2e406819c5dc6197a9a5c870 to fix the build on any
    non-amd64 architecture; patches upstream_Revert-cmake-issue.patch,
    and upstream_cmake-drop-bogus-CMAKE_TRY_COMPILE_TARGET_TYPE.patch.

 -- Pino Toscano <pino@debian.org>  Thu, 16 Feb 2023 00:08:31 +0100

skrooge (2.28.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.1, no changes required.
  * Remove inactive Uploaders, adding myself as one to avoid leaving the source
    with no human maintainers.
  * Drop the removal of development .so symlinks, as they are no more shipped.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Mon, 01 Aug 2022 16:27:22 +0200

skrooge (2.27.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - bump cmake to 3.16.0
  * Drop the breaks/replaces for versions older than Debian Buster.
  * Update install files.
  * Unregister skrooge_unit.knsrc as conffile, as it is no more in /etc.

 -- Pino Toscano <pino@debian.org>  Sat, 22 Jan 2022 10:45:25 +0100

skrooge (2.26.1-1) unstable; urgency=medium

  * Team upload.

  [ Norbert Preining ]
  * New upstream release.
  * Update list of installed files.

  [ Pino Toscano ]
  * Use execute_after_dh_auto_install to avoid invoking dh_auto_install
    manually.
  * Bump Standards-Version to 4.6.0, no changes required.
  * Minor update to the lintian overrides.

 -- Pino Toscano <pino@debian.org>  Fri, 27 Aug 2021 07:03:48 +0200

skrooge (2.24.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.5.1, no changes required.
  * Bump the debhelper compatibility to 13:
    - switch the debhelper-compat build dependency to 13
    - stop passing --fail-missing to dh_missing, as it is the default now
  * Remove the explicit as-needed linking, as it is done by binutils now.
  * Update the build dependencies according to the upstream build system:
    - bump cmake to 2.8.12
    - bump Qt packages to 5.12.0
    - drop kgendesignerplugin, no more needed
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Sun, 06 Dec 2020 15:04:06 +0100

skrooge (2.23.0-1) unstable; urgency=medium

  * Team upload.

  [ John Scott ]
  * Suggest aqbanking-tools for using AqBanking as an input backend.

  [ Pino Toscano ]
  * New upstream release.
  * Add the upstream GPG signing key.
  * Add Rules-Requires-Root: no.

 -- Pino Toscano <pino@debian.org>  Mon, 20 Jul 2020 06:33:32 +0200

skrooge (2.22.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.5.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Fri, 10 Apr 2020 10:30:06 +0200

skrooge (2.21.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Change the python3 build dependency to python3:any, as Python is needed
    only as build tool.
  * Update the patches:
    - upstream_Fix-build-with-Qt-5.13.patch: drop, backported from upstream
  * Bump the debhelper compatibility to 12:
    - switch the debhelper build dependency to debhelper-compat 12
    - remove debian/compat

 -- Pino Toscano <pino@debian.org>  Sun, 01 Dec 2019 19:00:03 +0100

skrooge (2.20.0-2) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.4.1, no changes required.
  * Add the configuration for the CI on salsa.
  * Backport upstream commit 79aeb3f4d3cc01761d99af5868c7fb5101cf2258 to build
    with newer KF; patch upstream_Fix-build-with-Qt-5.13.patch.

 -- Pino Toscano <pino@debian.org>  Wed, 23 Oct 2019 08:03:49 +0200

skrooge (2.20.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.4.0, no changes required.
  * Explicitly add the gettext build dependency.

 -- Pino Toscano <pino@debian.org>  Mon, 08 Jul 2019 21:46:54 +0200

skrooge (2.18.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.3.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Tue, 12 Feb 2019 07:21:35 +0100

skrooge (2.17.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Qt build dependencies to 5.7.0, according to the upstream build
    system.

 -- Pino Toscano <pino@debian.org>  Mon, 17 Dec 2018 13:21:13 +0100

skrooge (2.16.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Disable the build of the Qt Designer plugins, which are not useful without
    development stuff:
    - pass -DSKG_DESIGNER=OFF to cmake
    - remove special casing for Qt plugins when removing .so symlinks
  * Bump Standards-Version to 4.2.1, no changes required.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Sun, 04 Nov 2018 23:53:16 +0100

skrooge (2.15.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.2.0, no changes required.
  * Use dh_python3 to calculate automatically the Python 3 dependency:
    - add the dh-python, and python3 build dependencies
    - use the python3 dh addon
    - invoke dh_python3 on the proper data directory in skrooge-common
    - use the ${python3:Depends} substvar in skrooge-common, removing the
      manual python3 dependency in skrooge

 -- Pino Toscano <pino@debian.org>  Thu, 23 Aug 2018 07:58:00 +0200

skrooge (2.14.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Switch Homepage to https.
  * Switch from --list-missing to --fail-missing for dh_missing.
  * Remove the unused libkf5kdelibs4support-dev build dependency.

 -- Pino Toscano <pino@debian.org>  Mon, 25 Jun 2018 21:43:36 +0200

skrooge (2.13.0-2) unstable; urgency=medium

  * Team upload.
  * Move the appdata file from skrooge-common to skrooge, where the desktop
    file is
    - add proper breaks/replaces
  * Update Vcs-* fields.

 -- Pino Toscano <pino@debian.org>  Sun, 27 May 2018 11:22:24 +0200

skrooge (2.13.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Switch Vcs-* fields to salsa.debian.org.
  * Bump Standards-Version to 4.1.4, no changes required.

 -- Pino Toscano <pino@debian.org>  Tue, 08 May 2018 21:27:56 +0200

skrooge (2.12.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the patches:
    - upstream_cmake-fix-KDocTools-variable-name.patch: drop,
      backported from upstream
  * Avoid removing .so symlinks of plugins.
  * Bump the debhelper compatibility to 11:
    - bump the debhelper build dependency to 11~
    - bump compat to 11
    - remove --parallel for dh, as now done by default

 -- Pino Toscano <pino@debian.org>  Sat, 10 Mar 2018 09:16:40 +0100

skrooge (2.11.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Simplify watch file, and switch it to https.
  * Backport upstream commit 4d1f422b65fee6252787441b344571d5de3dd425 to enable
    the build of the English documentation; patch
    upstream_cmake-fix-KDocTools-variable-name.patch.
  * Bump Standards-Version to 4.1.3, no changes required.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Wed, 07 Feb 2018 22:51:43 +0100

skrooge (2.10.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Remove Fathi Boudra from Uploaders. (Closes: #879433)
  * Bump Standards-Version to 4.1.1, no changes required.
  * Remove trailing whitespaces in changelog.

 -- Pino Toscano <pino@debian.org>  Mon, 04 Dec 2017 19:50:13 +0100

skrooge (2.9.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Remove the manual kio dependency, it is injected automatically now.
  * Do not install anymore as documentation the upstream README (since it
    describes only how to build), and TODO (since it is gone).
  * Bump Standards-Version to 4.1.0, no changes required.
  * Depend on python3, since there are Python 3 scripts.

 -- Pino Toscano <pino@debian.org>  Fri, 01 Sep 2017 07:51:30 +0200

skrooge (2.8.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Pino Toscano <pino@debian.org>  Sun, 18 Jun 2017 09:22:24 +0200

skrooge (2.7.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Pino Toscano <pino@debian.org>  Mon, 23 Jan 2017 07:53:30 +0100

skrooge (2.6.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Sat, 21 Jan 2017 09:53:32 +0100

skrooge (2.5.0-1) unstable; urgency=medium

  * Team upload.

  [ Pino Toscano ]
  * New upstream release:
    - fixes German translation issue (Closes: #750573)
  * Replace deprecated build dependencies:
    - kdoctools-dev -> libkf5doctools-dev
    - kio-dev -> libkf5kio-dev
  * Recommend poppler-utils, as pdftotext is used by the new PDF import plugin.
  * Depend on kio and kinit, to make sure that KIO slaves work.
    (Closes: #839609)

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Update install files.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 05 Nov 2016 11:46:50 -0300

skrooge (2.4.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Pino Toscano <pino@debian.org>  Sat, 25 Jun 2016 10:15:20 +0200

skrooge (2.4.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release: (Closes: #776202, #802981)
    - switches from Qt4 (using QtWebKit) to Frameworks (Closes: #784527)
    - the skrooge_less.svgz and skrooge_more.svgz icons are open now
      (Closes: #800898)
  * Update the build dependencies following the port to Frameworks:
    - remove kdelibs5-dev, kdepimlibs5-dev, libkactivities-dev,
      libgrantlee-dev, libqca2-dev, libqjson-dev, and libqtwebkit-dev
    - add extra-cmake-modules, qtbase5-dev, qtbase5-private-dev,
      libqt5svg5-dev, qttools5-dev, libqt5webkit5-dev, qtdeclarative5-dev,
      libkf5activities-dev, libkf5archive-dev, libkf5completion-dev,
      libkf5config-dev, libkf5configwidgets-dev, libkf5coreaddons-dev,
      libkf5dbusaddons-dev, libkf5kdelibs4support-dev, kdoctools-dev,
      kgendesignerplugin, libkf5guiaddons-dev, libkf5i18n-dev,
      libkf5iconthemes-dev, libkf5itemviews-dev, libkf5jobwidgets-dev,
      kio-dev, libkf5newstuff-dev, libkf5notifications-dev,
      libkf5notifyconfig-dev, libkf5parts-dev, libkf5runner-dev,
      libkf5wallet-dev, libkf5widgetsaddons-dev, libkf5windowsystem-dev,
      libkf5xmlgui-dev, libgrantlee5-dev, and libqca-qt5-2-dev
  * Replace the libsqlite3-dev build dependency with libsqlcipher-dev.
  * Use the right dh addon:
    - switch from kde to kf5 dh addon
    - bump the pkg-kde-tools build dependency to >= 0.15.16
  * Disable the build and run of the tests for now, since it is not easy to
    run them.
  * Update install files.
  * Stop installing .so (unversioned) symlinks.
  * Update lintian overrides.
  * Update Vcs-Browser field.
  * Bump Standards-Version to 3.9.8, no changes required.
  * Link in as-needed mode.
  * Update/simplify watch file.
  * Remove unused libboost-dev build dependency.
  * Update the runtime dependencies:
    - replace libqca2-plugin-ossl with libqca-qt5-2-plugins
    - replace libqt4-sql-sqlite with libqt5sql5-sqlite

 -- Pino Toscano <pino@debian.org>  Tue, 21 Jun 2016 20:49:40 +0200

skrooge (1.9.3-1) unstable; urgency=low

  * New upstream release

  [ Cyril Brulebois ]
  * Specify a minimal version for libgrantlee-dev (>= 0.2), and
  libqjson-dev (>= 0.8.0) in Build-Depends, matching the contents
  of CMakeLists.txt (Closes: #746590)

  [ Mark Purcell ]
  * Update Standards-Version: no further changes

 -- Mark Purcell <msp@debian.org>  Sat, 12 Jul 2014 14:54:44 +1000

skrooge (1.9.0-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Fri, 18 Apr 2014 10:57:18 +1000

skrooge (1.8.0-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 12 Oct 2013 16:16:54 +1100

skrooge (1.7.4-1) unstable; urgency=low

  * New upstream release
  * update debian/watch - thanks Bart
  * Add Build-Depends: libqjson-dev

 -- Mark Purcell <msp@debian.org>  Sat, 06 Jul 2013 21:30:47 +1000

skrooge (1.7.2-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sun, 16 Jun 2013 09:05:25 +1000

skrooge (1.7.1-1) unstable; urgency=low

  * New upstream release
  * Update Vcs-Svn: - fix vcs-field-not-canonical
  * Update Standards-Version: 3.9.4 - no further changes

 -- Mark Purcell <msp@debian.org>  Sun, 12 May 2013 17:48:36 +1000

skrooge (1.6.3-1) experimental; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Tue, 09 Apr 2013 07:13:21 +1000

skrooge (1.6.0-1) experimental; urgency=low

  * Relax Build-Depends: libboost-dev
  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 02 Mar 2013 09:40:26 +1100

skrooge (1.4.0-2) experimental; urgency=low

  * Add missing Build-Depends: kdepimlibs5-dev, libkactivities-dev,
    xsltproc, libboost1.49-dev

 -- Mark Purcell <msp@debian.org>  Thu, 27 Dec 2012 10:57:03 +1100

skrooge (1.4.0-1) experimental; urgency=low

  * New upstream release
  * skrooge.install added usr/share/akonadi/
  * debian/rules cleanup

 -- Mark Purcell <msp@debian.org>  Mon, 24 Dec 2012 09:37:10 +1100

skrooge (1.3.3-1) experimental; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 08 Sep 2012 16:19:46 +1000

skrooge (1.3.0-1) unstable; urgency=low

  * New upstream release
    - Fixes "Skrooge crashes when selecting report" (Closes: #660325)

 -- Mark Purcell <msp@debian.org>  Sun, 06 May 2012 16:26:26 +1000

skrooge (1.2.0-2) unstable; urgency=low

  * Fix pinuparts dependency does-not-exist
  * Drop obsolete skrooge-common ${shlibs:Depends}
  * Update lintian-overrides non-dev-pkg-with-shlib-symlink

 -- Mark Purcell <msp@debian.org>  Wed, 25 Jan 2012 03:39:31 +1100

skrooge (1.2.0-1) unstable; urgency=low

  * lca2012 release

  * New upstream release
  * Drop patches/BUG288683 - included upstream

 -- Mark Purcell <msp@debian.org>  Mon, 16 Jan 2012 15:48:39 +1100

skrooge (1.1.1-1) unstable; urgency=low

  * New upstream release.
  * Add BUG288683_arch-dependent-file-in-usr-share.patch from upstream.

 -- Fathi Boudra <fabo@debian.org>  Sat, 31 Dec 2011 12:00:02 +0200

skrooge (1.0.0-1) unstable; urgency=low

  * New upstream release (LP: #869529)
  * Updated skrooge.install
    - Fixes "[skrooge] Import failed" (Closes: #632946)
  * Add Build-Depends: libgrantlee-dev
  * Update Vcs-Browser: anonscm.debian.org
  * Depends: skrooge-common (= ${source:Version})

 -- Mark Purcell <msp@debian.org>  Sun, 09 Oct 2011 21:52:39 +1100

skrooge (0.9.92-1) unstable; urgency=low

  * New upstream release
  * Add Build-Depends: libqtwebkit-dev
    - Fixes "FTBFS: fatal error: QWebView:" (Closes: #630205)

 -- Mark Purcell <msp@debian.org>  Tue, 28 Jun 2011 00:59:57 +1000

skrooge (0.9.0-1) unstable; urgency=low

  * New upstream release.

  [ Fathi Boudra ]
  * Update debian/control: add libqca2-plugin-ossl dependency.
    (Closes: #598127)

  [ Mark Purcell ]
  * Drop ftbfs_arm.patch - fixed upstream

 -- Mark Purcell <msp@debian.org>  Fri, 10 Jun 2011 22:56:44 +1000

skrooge (0.8.0.4-2) unstable; urgency=low

  * Add patch to fix build on ARM. (Closes: #615012)

 -- Fathi Boudra <fabo@debian.org>  Mon, 28 Feb 2011 15:45:07 +0200

skrooge (0.8.0.4-1) unstable; urgency=low

  * New upstream release.

  [ Scott Kitterman ]
  * Add debian/patches/01_arm_compile_fix.diff from Kubuntu to fix FTBFS on
    armel (Closes: #598966)

  [ Mark Purcell ]
  * Drop arm_compile_fix - included upstream

  [ Fathi Boudra ]
  * Update debian/skrooge-common.install file.

 -- Mark Purcell <msp@debian.org>  Sat, 29 Jan 2011 18:29:48 +1100

skrooge (0.7.3.2-1) experimental; urgency=low

  * New upstream release.

 -- Mark Purcell <msp@debian.org>  Wed, 22 Sep 2010 06:44:06 +1000

skrooge (0.7.2.2-1) unstable; urgency=low

  * New upstream release:
    - Correction bug 213786: Account balance is not displayed

 -- Mark Purcell <msp@debian.org>  Sat, 31 Jul 2010 12:27:41 +1000

skrooge (0.7.2-1) unstable; urgency=low

  * New upstream release.
  * Add myself to uploaders.

 -- Mark Purcell <msp@debian.org>  Sun, 25 Jul 2010 15:19:15 +1000

skrooge (0.7.1-2) unstable; urgency=low

  * Update location of KDE 4 HTML documentation.

 -- Fathi Boudra <fabo@debian.org>  Wed, 02 Jun 2010 01:04:52 +0300

skrooge (0.7.1-1) unstable; urgency=low

  * New upstream release.
  * Drop 97_fix_target_link_libraries.diff - merged upstream.
  * Update debian/control:
    - Bump kdelibs5-dev build-dependency.
    - Add libqt4-sql-sqlite dependency. (Closes: #582185)
  * Add debian/watch file. Thanks to Felix Geyer. (Closes: #582184)

 -- Fathi Boudra <fabo@debian.org>  Wed, 19 May 2010 08:40:03 +0300

skrooge (0.7.0-1) unstable; urgency=low

  * Initial release. (Closes: #548219)

 -- Fathi Boudra <fabo@debian.org>  Sun, 18 Apr 2010 12:30:46 +0300
